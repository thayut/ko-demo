Feature:
As a user of the app
I want to get a file

    Scenario: Create a file using given name and content
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "taltool.txt", "content": "this is the file content" }
        When I POST to /v1/files
        Then response code should be 200

    Scenario: Get a file using given file name in endpoint
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        When I GET /v1/files/taltool.txt
        Then response code should be 200
        Then response body should contain stats

    Scenario: Get a file using bad or no file name in endpoint fails
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        When I GET /v1/files/something
        Then response code should be 500
        Then response body should contain reason