Feature:
  As a user of the app
  I want to login

  Scenario: Create a user before login
    Given I set Content-Type header to application/json
    Given I set body to { "username": "test", "password": "test" }
    When I POST to /v1/createUser
    Then response code should be 200

  Scenario: Perform login using existing username and password
    Given I set Content-Type header to application/json
    Given I set body to { "username": "test", "password": "test" }
    When I POST to /v1/login
    Then response code should be 200
    Then response body should contain myToken

  Scenario: Perform login using non-existent username and password
    Given I set Content-Type header to application/json
    Given I set body to { "username": "someUser", "password": "somePassword" }
    When I POST to /v1/login
    Then response code should equal 401