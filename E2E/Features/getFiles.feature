Feature:
As a user of the app
I want to get a list of all files

    Scenario: Get a list of all text files
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        When I GET /v1/files
        Then response code should be 200
        Then response body should contain content
