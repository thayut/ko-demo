Feature:
As a user of the app
I want to create a file

    Scenario: Create a file using given name and content
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "taltool.txt", "content": "this is the file content" }
        When I POST to /v1/files
        Then response code should be 200

    Scenario: Create a file without content should fail
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "taltool.txt", "content": "" }
        When I POST to /v1/files
        Then response code should be 500

    Scenario: Create a file that is not .txt should fail
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "taltool.pdf", "content": "some content" }
        When I POST to /v1/files
        Then response code should be 500