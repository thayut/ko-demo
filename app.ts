import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import Cors from './APIs/Cors';
import { Routes } from './APIs/Routes';

const app: express.Application = express();

const port: number = 3000;

app.use(bodyParser());

try {
    if (process.env.NODE_ENV == 'dev') {
        mongoose.connect('mongodb://localhost:27017/Users');
    } else {
        mongoose.connect('mongodb://db:27017/Users');
    }

} catch (e) {
    console.error('Failed to connect to mongo!' + e);
}

let cors = new Cors();
let db = mongoose.connection;
let routes = new Routes(app);

db.on('error', (error) => {
    console.error(error);
});

db.once('open', () => {
    console.log('Connected!');
});

app.use('*', cors.headers);
app.use('/', routes.generalRoute);
app.get('/v1/files', routes.getFiles);
app.get('/v1/files/:file', routes.getFile);
app.post('/v1/login', routes.loginRoute);
app.post('/v1/createUser', routes.createUser);
app.post('/v1/files', routes.createFile);
app.put('/v1/files/:file', routes.changeFile);
app.delete('/v1/files', routes.deleteFile);

app.listen(port);
console.log(`Running on ${port}`);