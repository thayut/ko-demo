# KickOff Demo App! 

This is a simple app to manage and create files using a local API server and database, running through Docker.  
This app runs on localhost port 3000 (http://localhost:3000).

## Bootstrap

`git clone https://gitlab.com/thayut/ko-demo`  
`cd ko-demo`  
`npm run bootstrap`  

You should now have a working local api server with a mongoDB instance too! 

## Stop the services  
`npm stop`

## Run tests  
There are unit-tests and E2E tests integrated to the demo project.

### E2Es
`npm run e2e`

### Unit Tests
`npm run test`

# Interact with the API

Once the services are up and running - you will be able to interact with them using Postman.

### API Reference

## Authentication

In order to interact with the API, you must first authenticate by creating a new user:

#### POST /v1/createUser
create a new user!  
example body:
```json
{
  "username": "taltool",
  "password": "tooltal"
}
```
example response:  
```json
{
    "success": "created user taltool"
}
```

Once that's done, login with your username and password:

#### POST /v1/login
login!  
example body:
```json
{
  "username": "taltool",
  "password": "tooltal"
}
```  
example response:
```json
{
    "myToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MTc0NDA4NDl9.nkh_RPrXUqhrfgCb1NV-_KQAxzQWJ3Zd0ciYu_MhI9c"
}
```

## Files

Once you have authenticated, you should have received a jwt token.  
You need to add this token to a 'Authorization' Header for every http request.

#### GET /v1/files
get all files!  
example response:  
```json  
{
    "message": "list of files",
    "content": [
        {
            "fileName": "example.txt",
            "createdAt": "2018-01-31T23:15:47.314Z",
            "sizeInBytes": 12,
            "content": "hello world!"
        }
    ]
}  
```

#### GET /v1/files/:file
get a specific file by name in query URL  
example response:  
```json  
{
    "message": "list of files",
    "content": [
        {
            "fileName": "example.txt",
            "createdAt": "2018-01-31T23:15:47.314Z",
            "sizeInBytes": 12,
            "content": "hello world!"
        }
    ]
}  
```

#### POST /v1/files
create a file!  
example body:
```json
{
  "name": "taltool.txt",
  "content": "yo whats up?"
}
```
#### PUT /v1/files/:file
change an existing file!  :file is the filename  
example body:
```json
{
  "name": "taltool.txt",
  "content": "yo whats up with you?"
}
```
#### DELETE /v1/files
delete a file!  
example body:
```json
{
  "name": "taltool.txt"
}
```

