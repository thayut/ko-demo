import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import * as mongoose from 'mongoose';
import { UsersCollection } from "../Mongo/UsersCollection"
import { Files } from '../Files/Files';

const mySecret: string = 'I am the king';


class Routes {

    private user: any;

    public constructor(private app: express.Application) {
        this.user = new UsersCollection(mongoose);
        this.app = app;
    }

    public generalRoute = (req, res, next): void => {
        if (req.url == '/v1/login') {
            next();
            return;
        }

        if (req.url == '/v1/createUser') {
            next();
            return;
        }

        if (!req.get('Authorization')) {
            res.status(401).json({ "error": "no Authorization headers found!" });
            return;
        }

        if (!jwt.verify(req.get('Authorization'), mySecret)) {
            res.status(401).json({ "error": "bad token" });
            return;
        }
        next();
    };

    public getFiles = async (req, res): Promise<void> => {
        let file = new Files();
        try {
            let content = await file.listFiles();
            res.status(200).json({ "message": 'list of files', "content": content });

        } catch (e) {
            console.error(e);
            res.status(500).json({ "reason": "something went terribly wrong" });
        }
    };

    public loginRoute = async (req, res): Promise<string> => {
        let username: string = req.body.username;
        let password: string = req.body.password;

        if (!username || !password) {
            res.status(403).json({ "error": "username and password required" });
            return;
        }
        try {
            await this.user.checkUser(username, password);
            let myToken = jwt.sign({ username: username }, mySecret);
            console.log(`User ${username} authenticated successfully, sending token`);
            res.status(200).json({ myToken });

        } catch (e) {
            console.error(e);
            res.status(401).json({ "error": "Authorization failed!", "reason": e });
        }
    };

    public createUser = async (req, res): Promise<string> => {
        let username: string = req.body.username;
        let password: string = req.body.password;


        if (username.length <= 0 || password.length <= 0) {
            res.status(403).json({ "reason": "username or must password must not be empty!" });
            return;
        }

        try {
            await this.user.createUserIfNotExist(username, password);
            res.status(200).json({ "success": `created user ${username}` });
        }
        catch (e) {
            console.error(e);
            res.status(403).json({ "error": "User creation failed!", "reason": e })
        }
    };

    public getFile = async (req, res): Promise<any> => {
        let file = new Files();
        try {
            // console.log(req.params.file);
            let stats = await file.getFile(req.params.file);
            res.status(200).json({ "message": 'stats of file', "stats": stats });
        } catch (e) {
            console.error(e);
            res.status(500).json({ "error": "Something went terribly wrong", "reason": e });
        }
    };

    public deleteFile = async (req, res): Promise<any> => {
        let file = new Files();
        try {
            await file.deleteFile(req.body.name);
            res.status(200).json({ "message": `file ${req.body.name} removed successfully!` });
        }
        catch (e) {
            console.error(e);
            res.status(500).json({ "error": "Something went terribly wrong", "reason": e });
        }
    };
    
    public changeFile = async (req, res): Promise<any> => {
        let file = new Files();
        try {
            let content = await file.updateFile(req.body.name, req.body.content);
            res.status(200).json({ "message": `file ${req.body.name} modified successfully!`, "content": content });
        } catch (e) {
            console.error(e);
            res.status(500).json({ "error": "Something went terribly wrong", "reason": e });
        }
    };

    public createFile = async (req, res): Promise<any> => {
        let file = new Files();
        try {
            let content = await file.createFile(req.body.name, req.body.content);
            res.status(200).json({ "message": `file ${req.body.name} created successfully!`, "content": content });
        } catch (e) {
            // console.error(e);
            res.status(500).json({ "error": "Something went terribly wrong", "reason": e });
        }
    };

}

export { Routes }