export class Cors {
    headers(req, res, next) {
        let responseSettings = {
            "AccessControlAllowOrigin": "*",
            "AccessControlAllowHeaders": "x-access-token, Authorization, Authorization , Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
            "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
            "AccessControlAllowCredentials": true
        };

        res.header("Access-Control-Allow-Origin",  responseSettings.AccessControlAllowOrigin);
        res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
        res.header("Access-Control-Expose-Headers",  responseSettings.AccessControlAllowHeaders);
        res.header("Access-Control-Allow-Headers", responseSettings.AccessControlAllowHeaders);
        res.header("Access-Control-Allow-Methods", responseSettings.AccessControlAllowMethods);

        if ('OPTIONS' === req.method) {
            res.send(200);
        }
        else {
            next();
        }
    }
}

export default Cors