FROM node:boron

#create app directory
WORKDIR /usr/src/app

COPY package.json .

#install deps
RUN npm install

#bundle
COPY . .

EXPOSE 3000

CMD [ "npm", "run", "prod" ]