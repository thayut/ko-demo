interface IFile {
    fileName: string;
    sizeInBytes: number;
    createdAt: Date | string;
    content: string
}

export { IFile }