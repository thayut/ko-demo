import * as chai from 'chai';
import {FileValidator} from "./FileValidator"
import * as chaiPromised from 'chai-as-promised';

chai.use(chaiPromised);
chai.should();

let fileValidator;
let file = "taltool.txt";
let content = "content";

describe('Tests for FileValidator', () => {
    beforeEach(() => {
        fileValidator = new FileValidator();
    });

    describe('tests for validateFile', () => {











































































































































        














        it('should return a string array', () => {
            return fileValidator.validateFile(file, content).should.eventually.be.an('array').of.string;
        });

        it('should pass validation with file and content', () => {
            return fileValidator.validateFile(file, content).should.eventually.be.fulfilled;
        });

        it('should fail validation with no content', () => {
            return fileValidator.validateFile(file, '').should.eventually.be.rejected;
        });

        it('should return a content error in array if failed', () => {
            return fileValidator.validateFile(file, '').should.eventually.be.rejectedWith(['File must have content!']);
        });

        it('should return an extension error in array if failed', () => {
            return fileValidator.validateFile('taltool.pdf', content).should.eventually.be.rejectedWith(['Only .txt files are supported!']);
        });

        it('should return error array with both error msgs if failed both', () => {
            return fileValidator.validateFile('taltool.pdf', '').should.eventually.be.rejectedWith(['File must have content!', 'Only .txt files are supported!'])
        })
    });

});