import {Validator} from "./Validator"

class FileValidator extends Validator {

    constructor() {
        super();
    }

    public validateFile = (fileName, content): Promise<Array<string>> => {
        return new Promise((resolve, reject) => {

            let errorsResult: Array<string> = [];

            let con = this.validateContent(content);
            let ext = this.extensionValidator(fileName, 'txt');
            if (con && ext) {
                resolve(['File validated successfully']);
            } else {
                if (!con) {
                    errorsResult.push('File must have content!');
                }

                if (!ext) {
                    errorsResult.push('Only .txt files are supported!');
                }

                reject(errorsResult);
            }
        });
    };
}

export {FileValidator}